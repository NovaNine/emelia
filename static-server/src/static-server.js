const express = require('express');
const path = require('path');

const app = express();

// Port on which the server will listen
const SERVER_PORT = 4080;

// Serve the build game client files
app.use('/game/client', express.static(path.join(__dirname, '../../client/build')));
// Serve the static game client files
app.use('/game', express.static(path.join(__dirname, '../../client/public')));

// Start server
app.listen(SERVER_PORT, () => {
    console.log(`Static server listening on port ${SERVER_PORT}`);
});
